--c. The names of salespeople that have 2 or more orders. 
SELECT s.Name
FROM Salesperson AS s, Purchases AS p
WHERE s.s_id = p.saleperson_id
GROUP BY name, saleperson_id
HAVING COUNT(p.saleperson_id) > 1