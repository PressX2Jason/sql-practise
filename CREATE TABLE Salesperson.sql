IF OBJECT_ID('dbo.Salesperson', 'U') IS NOT NULL
	DROP TABLE dbo.Purchases, dbo.Salesperson

CREATE TABLE Salesperson
(
	s_id int,
	Name varchar(255) NOT NULL,
	Age int,
	Salary int,

	PRIMARY KEY(s_id),
	CHECK(Age>0),
);

INSERT INTO Salesperson
VALUES(1, 'Abe', 61, 140000)

INSERT INTO Salesperson
VALUES(2, 'Bob', 34, 44000)

INSERT INTO Salesperson
VALUES(5, 'Chris', 34, 40000)

INSERT INTO Salesperson
VALUES(7, 'Dan', 41, 52000)

INSERT INTO Salesperson
VALUES(8, 'Ken', 57, 115000)

INSERT INTO Salesperson
VALUES(11, 'Joe', 38, 38000)