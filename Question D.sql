/* Write a SQL statement to insert rows into a table called highAchiever(Name, Age),
 * where a salesperson must have a salary of 100,000 or greater to be included in the table.
 */
insert into highAchiever (name, age) 
(select name, age from salesperson where salary > 100000);