IF OBJECT_ID('dbo.Customer', 'U') IS NOT NULL
	IF OBJECT_ID('dbo.Purchases', 'U') IS NOT NULL
		DROP TABLE dbo.Purchases, dbo.Customer
	ELSE
		DROP TABLE dbo.Customer

CREATE TABLE Customer
(
	c_id int,
	Name varchar(255) NOT NULL,
	City varchar(255),

	IndustyType char(1),

	PRIMARY KEY(c_id),
);

INSERT INTO Customer
VALUES(4, 'Samsonic', 'pleasant', 'J')

INSERT INTO Customer
VALUES(6, 'Panasung', 'oaktown', 'J')

INSERT INTO Customer
VALUES(7, 'Samony', 'jackson', 'B')

INSERT INTO Customer
VALUES(9, 'Orange', 'Jackson', 'B')
