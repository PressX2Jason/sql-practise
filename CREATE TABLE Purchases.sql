IF OBJECT_ID('dbo.Purchases', 'U') IS NOT NULL
	DROP TABLE dbo.Purchases

CREATE TABLE Purchases
(
	Number int,
	order_date date,
	cust_id int,
	saleperson_id int,
	amount int,

	CHECK (amount > 0 ),
	PRIMARY KEY(Number),
	CONSTRAINT cust_id_in_order FOREIGN KEY(cust_id) REFERENCES Customer(c_id),
	CONSTRAINT sale_id_in_order FOREIGN KEY(saleperson_id) REFERENCES Salesperson(s_id),
);

INSERT INTO Purchases
VALUES (10, '1996-08-02', 4, 2, 540)

INSERT INTO Purchases
VALUES (20, '1999-01-30', 4, 8, 1800)


INSERT INTO Purchases
VALUES (30, '1995-07-14', 9, 1, 460)

INSERT INTO Purchases
VALUES (40, '1998-01-29', 7, 2, 2400)

INSERT INTO Purchases
VALUES (50, '1998-02-03',6, 7, 600)

INSERT INTO Purchases
VALUES (60, '1998-03-02', 6, 7, 720)

INSERT INTO Purchases
VALUES (70, '1998-05-06', 9, 7, 150)

